call plug#begin(stdpath('data') . '/plugged')
Plug 'https://github.com/tpope/vim-commentary.git'
Plug 'itchyny/lightline.vim'
Plug 'dense-analysis/ale'
Plug 'lervag/vimtex'
Plug 'tpope/vim-fugitive'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
call plug#end()

filetype plugin indent on

set autoindent
set noexpandtab
set tabstop=4
set shiftwidth=4

set foldcolumn=0
set number relativenumber
set nu rnu
set colorcolumn=80
set noshowmode
set list

set ttyfast
set lazyredraw

colorscheme industry

" Deoplete
let g:deoplete#enable_at_startup = 1

call deoplete#custom#var('omni', 'input_patterns', {
      \ 'tex': g:vimtex#re#deoplete
      \})

" VimTex
let g:vimtex_view_method = 'mupdf'
let g:vimtex_compiler_latexmk = {
    \ 'options' : [
    \   '-shell-escape',
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \ ],
    \}
