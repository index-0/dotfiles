export GOPATH=$HOME/.go
export PATH="$GOPATH/bin:$HOME/.local/bin:$PATH"

export VISUAL=nvim
export EDITOR=nvim
