{-# OPTIONS_GHC -Wall -optc-march=znver1 -O2 #-}

import Data.List
import Data.Semigroup
import XMonad
import XMonad.Actions.CycleWS
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.WindowSwallowing
import XMonad.Layout.Decoration
import XMonad.Layout.Grid
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoBorders
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Prompt
import XMonad.Prompt.AppendFile
import XMonad.Prompt.Man
import XMonad.Prompt.Shell
import XMonad.Prompt.Ssh
import XMonad.Prompt.Unicode
import XMonad.Util.ClickableWorkspaces
import XMonad.Util.Run
import qualified Data.Map as M
import qualified XMonad.StackSet as W

myFont :: [Char]
myFont = "xft:Cousine:style=Regular:pixelsize=13"

myNormalBorderColor :: [Char]
myNormalBorderColor = "#2e3440"
myFocusedBorderColor :: [Char]
myFocusedBorderColor = "#81a1c1"
myBackgroundColor :: [Char]
myBackgroundColor = "#2e3440"
myForegroundColor :: [Char]
myForegroundColor = "#d8dee9"

myWorkspaces :: [WorkspaceId]
myWorkspaces = [" www ", " dev ", " sys ", " eth ", " mpd ", " gfx ", " med ", " im ", " p2p ", " + "]

myKeys :: XConfig Layout -> M.Map (KeyMask, KeySym) (X ())
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)
    , ((modm .|. shiftMask, xK_n     ), appendFilePrompt myPromptConfig "/home/orpheus/.notes")
    , ((modm .|. shiftMask, xK_v     ), appendFilePrompt myPromptConfig "/home/orpheus/.reminders/default.rem")
    , ((modm .|. shiftMask, xK_d     ), shellPrompt myPromptConfig)
    , ((modm .|. shiftMask, xK_m     ), manPrompt myPromptConfig)
    , ((modm .|. shiftMask, xK_s     ), sshPrompt myPromptConfig)
    , ((modm .|. shiftMask, xK_u     ), unicodePrompt "/usr/share/unicode-data/UnicodeData.txt" myPromptConfig)
    , ((modm .|. shiftMask, xK_q     ), kill)
    , ((modm,               xK_space ), sendMessage NextLayout)
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
    , ((modm .|. shiftMask, xK_t     ), sendMessage $ JumpToLayout "Tabbed Simplest")
    , ((modm .|. shiftMask, xK_g     ), sendMessage $ JumpToLayout "Grid")
    , ((modm .|. shiftMask, xK_y     ), sendMessage $ JumpToLayout "ThreeCol")
    , ((modm .|. shiftMask, xK_x     ), sendMessage $ Toggle FULL)
    , ((modm,               xK_n     ), refresh)
    , ((modm,               xK_j     ), windows W.focusDown)
    , ((modm,               xK_k     ), windows W.focusUp)
    , ((modm,               xK_m     ), windows W.focusMaster)
    , ((modm,               xK_Return), windows W.swapMaster)
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown)
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp)
    , ((modm,               xK_h     ), sendMessage Shrink)
    , ((modm,               xK_l     ), sendMessage Expand)
    , ((modm .|. shiftMask, xK_z     ), toggleWS)
    , ((modm .|. shiftMask, xK_l     ), safeSpawn "slock" [])
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)
    , ((modm,               xK_comma ), sendMessage (IncMasterN 1))
    , ((modm,               xK_period), sendMessage (IncMasterN (-1)))
    , ((modm,               xK_b     ), sendMessage ToggleStruts)
    , ((modm,               xK_c     ), spawn "xmonad --recompile; killall -9 xmobar; xmonad --restart")

    ]

    ++

    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) $ [xK_1 .. xK_9] ++ [xK_0]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]

    ++

    [ ((modm,                     xK_r), spawn "sh $HOME/.xmonad/scripts/remind.sh")
--    , ((modm,                     xK_f), spawn "firefox -P secure --new-instance")
--    , ((modm .|. shiftMask,       xK_f), spawn "firefox -P default --new-instance")
    , ((   0, 0x1008ff11), spawn "amixer -D pulse sset 'Master' 5%-")
    , ((modm, 0x1008ff11), spawn "amixer -D pulse sset 'Master' 1%-")
    , ((   0, 0x1008ff12), spawn "amixer -D pulse set Master 1+ toggle")
    , ((   0, 0x1008ff13), spawn "amixer -D pulse sset 'Master' 5%+")
    , ((modm, 0x1008ff13), spawn "amixer -D pulse sset 'Master' 1%+")
    , ((   0, 0x1008ff14), spawn "mpc toggle")
    , ((   0, 0x1008ff15), spawn "mpc stop")
    , ((   0, 0x1008ff16), spawn "mpc prev")
    , ((modm, 0x1008ff16), spawn "mpc seek -00:00:05")
    , ((   0, 0x1008ff17), spawn "mpc next")
    , ((modm, 0x1008ff17), spawn "mpc seek +00:00:05")

    , ((                 0, 0xff61), spawn "scrot -q 80 -b ~/pictures/screenshots/%Y-%m-%d-%T-screenshot.png && notify-send \"Fullscreen capture saved as:\" \"`date +%Y`-`date +%m`-`date +%d`-`date +%T`-screenshot.png\"")
    , ((              modm, 0xff61), spawn "scrot -q 80 -u ~/pictures/screenshots/%Y-%m-%d-%T-screenshot.png && notify-send \"Window capture saved as:\" \"`date +%Y`-`date +%m`-`date +%d`-`date +%T`-screenshot.png\"")
    , ((modm .|. shiftMask, 0xff61), spawn "import ~/pictures/screenshots/$(date '+%Y-%m-%d-%T')-screenshot.png && notify-send \"Rectangle area capture saved as:\" \"`date +%Y`-`date +%m`-`date +%d`-`date +%T`-screenshot.png\"")
    , ((                       controlMask, 0xff61), spawn "scrot -q 80 -b -e 'xclip -selection clipboard -target image/png -i $f' && notify-send \"Fullscreen capture copied to clipboard.\"")
    , ((              modm .|. controlMask, 0xff61), spawn "scrot -q 80 -u -e 'xclip -selection clipboard -target image/png -i $f' && notify-send \"Window capture copied to clipboard.\"")
    , ((modm .|. shiftMask .|. controlMask, 0xff61), spawn "import png:- | xclip -selection clipboard -t image/png && notify-send \"Rectangle area capture copied to clipboard\"")]

myMouseBindings :: XConfig l -> M.Map (KeyMask, Button) (Window -> X ())
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))
    ]

myLayout = avoidStruts . smartBorders . mkToggle (NOBORDERS ?? FULL ?? EOT) $
  Tall 1 (3/100) (1/2) |||
  Grid |||
  ThreeCol 1 (3/100) (1/2) |||
  tabbed shrinkText myTabbedTheme

--winCount = length . W.index . windowset <$> get
--winCount = gets (windowset) >>= return . length . W.index
--winCount = gets (W.index . windowset) >>= return . length

---winCount :: X Int
---winCount = gets (W.index . windowset) >>= return . length

myLayoutPrinter :: String -> String
myLayoutPrinter "Tall"            = " :tall "
myLayoutPrinter "Full"            = " :full "
myLayoutPrinter "Grid"            = " :grid "
myLayoutPrinter "ThreeCol"        = " :tcol "
myLayoutPrinter "Tabbed Simplest" = " :tabs "
myLayoutPrinter x                 = x

myManageHook :: Query (Endo WindowSet)
myManageHook = composeAll
    [  className =? "MPlayer"             --> doFloat
    ,  className =? "Gimp"                --> doShift "*"
    ,  resource  =? "desktop_window"      --> doIgnore
    ,  resource  =? "kdesktop"            --> doIgnore
    ,  resource  =? "main"                --> doCenterFloat
    ,  title     =? "main"                --> doCenterFloat]

myHandleEventHook :: Event -> X All
myHandleEventHook = swallowEventHook (className =? "st-256color") (return True)

myStartupHook :: MonadIO m => m ()
myStartupHook =
        spawn "xsetroot -cursor_name left_ptr"

myPromptConfig :: XPConfig
myPromptConfig = def
    { font = myFont
    , bgColor = myBackgroundColor
    , fgColor = myForegroundColor
    , fgHLight = "#88c0d0"
    , bgHLight = myBackgroundColor
    , promptBorderWidth = 0
    , position = Top
    , alwaysHighlight = True
    , height = 16
    , defaultText = ""
    , searchPredicate = isInfixOf
}

myTabbedTheme :: Theme
myTabbedTheme = def
    { activeColor = "#2e3440"
    , inactiveColor = "#4c566a"
    , urgentColor = "#4c566a"
    , activeBorderWidth = 0
    , inactiveBorderWidth = 0
    , urgentBorderWidth = 0
    , activeTextColor = "#88c0d0"
    , inactiveTextColor = "#d8dee9"
    , urgentTextColor = "#bf616a"
    , fontName = myFont
}

main :: IO ()
main =
    spawnPipe "xmobar .xmonad/xmobarsp" >>
    spawnPipe "xmobar .xmonad/xmobarrc" >>=
    \ xmobarrc -> xmonad $ docks . ewmh $ def {
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,
        terminal           = "st",
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myHandleEventHook,
        workspaces         = myWorkspaces,
        modMask            = mod4Mask,
        keys               = myKeys,
        mouseBindings      = myMouseBindings,
        borderWidth        = 1,
        logHook            = clickablePP def {
                      ppCurrent         = xmobarColor "#81a1c1" "" . wrap "<box type=Top width=1 color=#81a1c1>" "</box>"
                    , ppHidden          = xmobarColor "#d8dee9" ""
                    , ppHiddenNoWindows = xmobarColor "#4c566a" ""
                    , ppTitle           = xmobarColor "#88c0d0" "" . pad
                    , ppSep             = xmobarColor "#ffffff" "" ""
                    , ppUrgent          = xmobarColor "#bf616a" "" . wrap "!" "!"
                    , ppWsSep           = ""
                    , ppOutput          = hPutStrLn xmobarrc
                    , ppLayout          = xmobarColor "#b48ead" "" . myLayoutPrinter
                      } >>= dynamicLogWithPP,
        startupHook        = myStartupHook,
        focusFollowsMouse  = True
        }
