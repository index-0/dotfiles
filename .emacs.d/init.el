(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(setq backup-directory-alist `(("." . "~/.emacs.d/saves/bak")))
(setq backup-by-copying t)
(setq delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t)

(savehist-mode)
(setq-default savehist-file "~/.emacs.d/tmp/saves/hist")

(fido-mode)
(icomplete-mode)

(xterm-mouse-mode 1)
(global-set-key (kbd "<mouse-4>") 'scroll-down-line)
(global-set-key (kbd "<mouse-5>") 'scroll-up-line)
;; (global-set-key (kbd "<mouse-6>") 'scroll-left)
;; (global-set-key (kbd "<mouse-7>") 'scroll-right)

(setq inhibit-startup-message t)
(setq initial-scratch-message nil)
(menu-bar-mode -1)
(tool-bar-mode -1)
(show-paren-mode 1)
;; (load-theme 'zenburn t)

(setq frame-title-format "emacs: %b")

(require 'display-line-numbers)
(global-display-line-numbers-mode)
(setq display-line-numbers-type 'relative)

(global-display-fill-column-indicator-mode)
(setq-default display-fill-column-indicator-column '79)
(setq-default show-trailing-whitespace t)

;; FlyCheck
(global-flycheck-mode)
(setq-default flycheck-indication-mode 'right-fringe)

;; FlySpell
(defun flyspell-spanish ()
  (interactive)
 (ispell-change-dictionary "castellano")
  (flyspell-buffer))

(defun flyspell-english ()
  (interactive)
  (ispell-change-dictionary "default")
  (flyspell-buffer))

;; C
(require 'cc-mode)

(setq c-default-style "linux"
      c-basic-offset 4)

(require 'ggtags)
(add-hook 'c-mode-common-hook
	  (lambda ()
	    (when (derived-mode-p 'c-mode 'c++-mode 'java-mode 'asm-mode)
	      (ggtags-mode 1))))

(define-key ggtags-mode-map (kbd "C-c g s") 'ggtags-find-other-symbol)
(define-key ggtags-mode-map (kbd "C-c g h") 'ggtags-view-tag-history)
(define-key ggtags-mode-map (kbd "C-c g r") 'ggtags-find-reference)
(define-key ggtags-mode-map (kbd "C-c g f") 'ggtags-find-file)
(define-key ggtags-mode-map (kbd "C-c g c") 'ggtags-create-tags)
(define-key ggtags-mode-map (kbd "C-c g u") 'ggtags-update-tags)

(define-key ggtags-mode-map (kbd "M-,") 'pop-tag-mark)

(require 'company)
(add-hook 'after-init-hook 'global-company-mode)

(setq company-backends (delete 'company-semantic company-backends))
(define-key c-mode-map  [(tab)] 'company-complete)
(define-key c++-mode-map  [(tab)] 'company-complete)

(add-to-list 'company-backends 'company-c-headers)

(semantic-mode 1)
(global-semantic-idle-summary-mode 1)

;; Haskell Config
(eval-after-load "haskell-mode"
  '(define-key haskell-mode-map (kbd "C-c C-c") 'haskell-compile))

(eval-after-load "haskell-cabal"
      '(define-key haskell-cabal-mode-map (kbd "C-c C-c") 'haskell-compile))

;; AUCTeX
(with-eval-after-load 'tex
  (setq-default LaTeX-command "latex -shell-escape")
  (add-to-list 'TeX-view-program-selection '(output-pdf "Zathura")))
